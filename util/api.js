import axios from 'axios';
import { withNetworkConnectivity } from 'react-native-offline';

export const API_URL = 'http://centralcatalogue.com:3100/'
export const withCheckInternet = withNetworkConnectivity({
    pingServerUrl: API_URL,
});

const api = axios.create({
    baseURL: API_URL,
});

export default api;