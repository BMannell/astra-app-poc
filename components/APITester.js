import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';
import { ConnectivityRenderer } from 'react-native-offline';
import api from '../util/api';

export default class APITester extends Component {

  constructor(props) {
    super(props);
    this.state = { todos: [] };
  }

  onButtonPress = () => {
    console.log('calling api')
    let self = this;
    api
      .get('/todos')
      .then(response => self.setState({ todos: response.data }))
      .catch(error => console.log(error))
  }

  setTodoList = (data) => {
    this.setState({ todos: data });
  }

  render() {
    return (
      <View style={{ padding: 15 }}>
        <ConnectivityRenderer>
          {isConnected => (
            isConnected ? (
              <Button style={{ fontSize: 20 }} onPress={this.onButtonPress} title='Get Todos' />
            ) : (
                <Text style={{ fontSize: 20 }}>Welcome to nothing ya scrub.</Text>
              )
          )}
        </ConnectivityRenderer>
        <View>
          {this.state.todos.map((item, index) => {
            return <Text key={item.id}>{ item.name }</Text>
          })}
        </View>
      </View>
    );
  }
}