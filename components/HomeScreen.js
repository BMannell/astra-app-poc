import React, { Component } from 'react';
import { Text, Button, View, Dimensions } from 'react-native';

export default class HomeScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, padding: 15 }}>
        <Text>Welcome to the Astra App Proof of Concept!</Text>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 1, padding: 5 }}>
            <Button
              title="Go to Hello"
              onPress={() => this.props.navigation.navigate('Hello')}
            />
          </View>
          <View style={{ flex: 1, padding: 5 }}>
            <Button
              title="Go to ToDoList"
              onPress={() => this.props.navigation.navigate('ToDoList')}
            />
          </View>
        </View>
      </View>
    );
  }
}