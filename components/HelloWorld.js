import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { ConnectivityRenderer } from 'react-native-offline';

export default class HelloWorld extends Component {
  render() {
    return (
      <View style={{ padding: 15 }}>
      <ConnectivityRenderer>
      {isConnected => (
        isConnected ? (
          <Text style={{ fontSize: 20}}>Hello, world wide web!</Text>
        ) : (
          <Text style={{ fontSize: 20}}>Welcome to nothing ya scrub.</Text>
        )
      )}
      </ConnectivityRenderer>
      </View>
    );
  }
}