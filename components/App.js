/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {
  Component
} from 'react';

import {
  Platform,
  YellowBox
} from 'react-native';

import { createNetworkMiddleware, withNetworkConnectivity } from 'react-native-offline';

import Icon from 'react-native-vector-icons/FontAwesome';

import {
  createBottomTabNavigator
} from 'react-navigation';

import {
  applyMiddleware,
  createStore
} from 'redux';
import {
  Provider
} from 'react-redux';
import { createLogger } from 'redux-logger'
import Thunk from 'redux-thunk';


import HelloWorld from './HelloWorld';
import HomeScreen from './HomeScreen';
import ToDoList from '../todos/component';
import APITester from './APITester';

import reducer from '../reducers';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

const networkMiddleware = createNetworkMiddleware({
  regexActionType: /GET|POST|DELETE|CREATE|PATCH/
});
const logger = createLogger({
  colors: {
    title: false,
    prevState: false,
    action: false,
    nextState: false,
    error: false
  }
})
const store = createStore(
  reducer,
  applyMiddleware(...[
    networkMiddleware,
    logger,
    Thunk
  ])
)

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const RootStack = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Home',
      tabBarIcon: ({ focused, tintColor }) => {
        return <Icon name="home" size={20} color={tintColor} />;
      }
    }
  },
  HelloWorld: {
    screen: HelloWorld,
    navigationOptions: {
      title: 'Hello',
      tabBarIcon: ({ focused, tintColor }) => {
        return <Icon name="globe" size={20} color={tintColor} />;
      }
    }
  },
  ToDoList: {
    screen: ToDoList,
    navigationOptions: {
      title: 'Todos',
      tabBarIcon: ({ focused, tintColor }) => {
        return <Icon name="list-ul" size={20} color={tintColor} />;
      }
    }
  },
  APITester: {
    screen: APITester,
    navigationOptions: {
      title: 'API',
      tabBarIcon: ({ focused, tintColor }) => {
        return <Icon name="server" size={20} color={tintColor} />;
      }
    }
  }
}, {
    initialRouteName: 'ToDoList',
  });

RootStack = withNetworkConnectivity({
  withRedux: true // It won't inject isConnected as a prop in this case
})(RootStack);

export default class App extends Component {

  //  componentWillMount() {
  //    const user = { uuid: DeviceInfo.getUniqueID() };
  //
  //    if (!store.getState().user.data) {
  //      store.dispatch(setLocalUser(user));
  //    }
  //
  //    store.dispatch(getAndSetUser(user));
  //    // Retry Images if some are pending
  //    store.dispatch(retryImages(selectPendingImages()(store.getState())));
  //  }

  render() {
    return (<Provider store={store}>
      <RootStack />
    </Provider>
    );
  }
}