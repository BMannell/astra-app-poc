'use strict';

import React from 'React'
import ReactTestRenderer from 'react-test-renderer'
import HelloWorld from '../HelloWorld.js'

describe('HelloWorld', () => {
  it('renders correctly', () => {
    const instance = ReactTestRenderer.create(
      <HelloWorld></HelloWorld>
    );
    expect(instance.toJSON()).toMatchSnapshot();
  });
});