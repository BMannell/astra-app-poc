import api from '../util/api';

export const getTodo = (todo) => api.get(`/todos/${todo.id}`);

export const getAllTodos = () => api.get(`/todos`);

export const createTodo = (todo) =>
  api.post('/todos', {
    todo: {
      complete: todo.complete
    }
  });

export const updateTodo = (todo) =>
  api.patch('/todos/${todo.id}', {
    todo: {
      complete: todo.complete
    }
  });