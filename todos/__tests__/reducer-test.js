import reducer from '../reducer'
import * as types from '../types'

describe('todos reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      'list': []
    })
  })
  it('should handle CREATE_TODOS_SUCCESS', () => {
    let payload = {
      id: 1,
      text: 'Test Todo',
      complete: true
    }
    expect(
      reducer(undefined, {
        type: types.CREATE_TODO_SUCCESS,
        payload
      })
    ).toEqual({
      "list": [{
        id: 1,
        text: 'Test Todo',
        complete: true
      }]
    })
    expect(
      reducer({
        'list': [{
          'text': 'First Todo',
          'completed': false,
          'id': 2
        }]
      }, {
        type: types.CREATE_TODO_SUCCESS,
        payload
      })
    ).toEqual({
      'list': [{
        'text': 'First Todo',
        'completed': false,
        'id': 2
      }, {
        id: 1,
        text: 'Test Todo',
        complete: true
      }]
    })
  })

  it('should handle DELETE_TODO_SUCCESS', () => {
    expect(
      reducer({
        'list': [{
            'text': 'First Todo',
            'completed': false,
            'id': 1
          },
          {
            'text': 'Second Todo',
            'completed': false,
            'id': 2
          }
        ]
      }, {
        type: types.DELETE_TODO_SUCCESS,
        payload: 1
      })
    ).toEqual({
      'list': [{
        'text': 'Second Todo',
        'completed': false,
        'id': 2
      }]
    })
  })

})