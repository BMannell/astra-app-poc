import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../actions.js'
import * as types from '../types.js'
import api from '../../util/api'
import MockAdapter from 'axios-mock-adapter';

const mockStore = configureMockStore([thunk])
const mock = new MockAdapter(api);

describe('actions', () => {
  afterEach(() => {
    mock.reset()
  })

  it('creates GET_TODOS_SUCCESS when fetching todos has been done', () => {
    let mockTodosList = [{
      id: 1,
      text: 'do something',
      complete: false
    }]
    mock.onGet('/todos').reply(200, mockTodosList);
    const expectedActions = [{
        type: types.GET_TODOS_REQUEST
      },
      {
        type: types.GET_TODOS_SUCCESS,
        payload: mockTodosList
      }
    ]
    const store = mockStore({
      todos: {
        list: []
      }
    })
    return store.dispatch(actions.getTodos()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates GET_TODOS_FAILED when fetching todos has failed', () => {
    mock.onGet('/todos').reply(400);
    const expectedActions = [{
        type: types.GET_TODOS_REQUEST
      },
      {
        type: types.GET_TODOS_FAILED
      }
    ]
    const store = mockStore({
      todos: {
        list: []
      }
    })
    return store.dispatch(actions.getTodos()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates DELETE_TODO_SUCCESS when delete todo succeeds', () => {
    mock.onDelete('/todos/1').reply(200);
    const expectedActions = [{
        type: types.DELETE_TODO_REQUEST
      },
      {
        type: types.DELETE_TODO_SUCCESS,
        payload: 1
      }
    ]
    const store = mockStore({
      todos: {
        list: [{
          id: 1,
          text: 'do something',
          complete: false
        }]
      }
    })
    return store.dispatch(actions.deleteTodo(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates DELETE_TODO_FAILED when request has failed', () => {
    mock.onDelete('/todos/1').reply(400);
    const expectedActions = [{
        type: types.DELETE_TODO_REQUEST
      },
      {
        type: types.DELETE_TODO_FAILED
      }
    ]
    const store = mockStore({
      todos: {
        list: [{
          id: 1,
          text: 'do something',
          complete: false
        }]
      }
    })
    return store.dispatch(actions.deleteTodo(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates CREATE_TODO_SUCCESS when create todo succeeds', () => {
    mock.onPost('/todos').reply(200, {
      id: 1,
      text: 'New Todo',
      complete: false
    });
    const expectedActions = [{
        type: types.CREATE_TODO_REQUEST
      },
      {
        type: types.CREATE_TODO_SUCCESS,
        payload: {
          id: 1,
          text: 'New Todo',
          complete: false
        }
      }
    ]
    const store = mockStore({
      todos: {
        list: []
      }
    })
    return store.dispatch(actions.createTodo()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates CREATE_TODO_FAILED when request has failed', () => {
    mock.onPost('/todos').reply(400);
    const expectedActions = [{
        type: types.CREATE_TODO_REQUEST
      },
      {
        type: types.CREATE_TODO_FAILED
      }
    ]
    const store = mockStore({
      todos: {
        list: [{
          id: 1,
          text: 'New Todo',
          complete: false
        }]
      }
    })
    return store.dispatch(actions.createTodo()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates UPDATE_TODO_SUCCESS when update todo succeeds', () => {
    mock.onPatch('/todos/1').reply(200, {
      id: 1,
      text: 'Change Todo',
      complete: false
    });
    const expectedActions = [{
        type: types.UPDATE_TODO_REQUEST
      },
      {
        type: types.UPDATE_TODO_SUCCESS,
        payload: {
          id: 1,
          text: 'Change Todo',
          complete: false
        }
      }
    ]
    const store = mockStore({
      todos: {
        list: [{
          id: 1,
          text: 'do something',
          complete: false
        }]
      }
    })
    return store.dispatch(actions.updateTodo(1, {
      text: 'Changed Todo',
      complete: false
    })).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates UPDATE_TODO_FAILED when request has failed', () => {
    mock.onPatch('/todos/1').reply(400);
    const expectedActions = [{
        type: types.UPDATE_TODO_REQUEST
      },
      {
        type: types.UPDATE_TODO_FAILED
      }
    ]
    const store = mockStore({
      todos: {
        list: [{
          id: 1,
          text: 'do something',
          complete: false
        }]
      }
    })
    return store.dispatch(actions.updateTodo(1, {
      text: 'Changed Todo',
      complete: false
    })).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})