import * as types from './types'
import makeOfflineInterceptable from '../util/offlineHelper'
import api from '../util/api'

/** Index **/
export const getTodosRequest = () => {
  return {
    type: types.GET_TODOS_REQUEST
  }
}

export const getTodosSuccess = (payload) => {
  return {
    type: types.GET_TODOS_SUCCESS,
    payload
  }
}

export const getTodosFailed = (error) => {
  return {
    type: types.GET_TODOS_FAILED
  }
}

export const getTodos = () => {
  return makeOfflineInterceptable(
    (dispatch) => {
      dispatch(getTodosRequest())
      return api.get('/todos').then(({
          data
        }) => {
          dispatch(getTodosSuccess(data))
        })
        .catch((error) => {
          dispatch(getTodosFailed())
        })
    }
  )
}

/** Create **/
export const createTodo = (todo) => {
  return makeOfflineInterceptable(
    (dispatch) => {
      dispatch(createTodoRequest())
      return api.post('/todos', {
          todo
        }).then(({
          data
        }) => {
          dispatch(createTodoSuccess(data))
        })
        .catch((error) => {
          dispatch(createTodoFailed())
        })
    }
  )
}

export const createTodoRequest = () => {
  return {
    type: types.CREATE_TODO_REQUEST
  }
}

export const createTodoSuccess = (payload) => {
  return {
    type: types.CREATE_TODO_SUCCESS,
    payload
  }
}

export const createTodoFailed = (error) => {
  return {
    type: types.CREATE_TODO_FAILED
  }
}

/** Update **/
export const updateTodo = (id, todo) => {
  return makeOfflineInterceptable(
    (dispatch) => {
      dispatch(updateTodoRequest())
      return api.patch(`/todos/${id}`, {
          todo
        }).then(({
          data
        }) => {
          dispatch(updateTodoSuccess(data))
        })
        .catch((error) => {
          dispatch(updateTodoFailed())
        })
    }
  )
}

export const updateTodoRequest = () => {
  return {
    type: types.UPDATE_TODO_REQUEST
  }
}

export const updateTodoSuccess = (payload) => {
  return {
    type: types.UPDATE_TODO_SUCCESS,
    payload
  }
}

export const updateTodoFailed = (error) => {
  return {
    type: types.UPDATE_TODO_FAILED
  }
}

/** Delete **/
export const deleteTodo = (id) => {
  return makeOfflineInterceptable(
    (dispatch) => {
      dispatch(deleteTodoRequest())
      return api.delete(`todos/${id}`).then(({
          data
        }) => {
          dispatch(deleteTodoSuccess(id))
        })
        .catch((error) => {
          dispatch(deleteTodoFailed())
        })
    }
  )
}

export const deleteTodoRequest = () => {
  return {
    type: types.DELETE_TODO_REQUEST
  }
}

export const deleteTodoSuccess = (payload) => {
  return {
    type: types.DELETE_TODO_SUCCESS,
    payload
  }
}

export const deleteTodoFailed = (error) => {
  return {
    type: types.DELETE_TODO_FAILED
  }
}