import * as types from './types'
import {
  reject
} from 'lodash'

const initialState = {
  list: [],

}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_TODOS_SUCCESS:
      {
        return {
          ...state,
          list: [...state.list, ...action.payload]
        }
      }
    case types.CREATE_TODO_SUCCESS:
      {
        return {
          ...state,
          list: state.list.concat(action.payload)
        }
      }
    case types.UPDATE_TODO_SUCCESS:
      {
        return {
          ...state,
          list: state.list.map(todo => {
            if (todo.id == payload.id) {
              return payload
            } else {
              return todo
            }
          })
        }
      }
    case types.DELETE_TODO_SUCCESS:
      {
        return {
          ...state,
          list: reject(state.list, ['id', action.payload])
        }
      }
    default:
      return state;
  }
}