import React, { Component } from 'react';
import { View, ScrollView, Button, Text, TextInput } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CheckBox from 'react-native-check-box'
import { reduce, forEach } from 'lodash'
import { ConnectivityRenderer } from 'react-native-offline';
import { createTodo, updateTodo, deleteTodo, getTodos } from './actions';

class ToDoList extends Component {

  constructor(props) {
    super(props);
    this.state = { text: '' };
  }

  componentWillMount = () => {
    this.props.getTodos();
  }

  getInput = () => {
    if (this.state.text != '') {
      this.props.createTodo({ text: this.state.text });
      this.setState({ text: '' })
    }
  }

  getCompletedToDoCount = () => {
    return reduce(this.props.todos.list, (count, todo) => {
      if (todo.completed) {
        count++
      }
      return count;
    }, 0)
  }

  toggleTodo = (todo) => {
    this.props.updateTodo(todo.id, { text: todo.text, complete: !todo.complete })
  }

  onPressRemoveButton = () => {
    let ids = reduce(this.props.todos.list, (completeList, todo) => {
      console.log('reducing', completeList, todo);
      if (todo.completed) {
        completeList = completeList.concat(todo.id)
      }
      return completeList;
    }, []);

    forEach(ids, (todoId) => {
      this.props.removeTodo(todoId)
    });
  }

  render() {
    return (
      <ConnectivityRenderer>
        {isConnected => (
          isConnected ? (
            <View style={{ flex: 1, padding: 15, flexDirection: 'column' }}>
              <View style={{ height: 40, marginBottom: 10, flexDirection: 'row' }}>
                <TextInput
                  style={{ flex: 1 }}
                  onChangeText={(text) => this.setState({ text })}
                  value={this.state.text}
                  placeholder='Add a to-do'
                />
                <Button
                  onPress={this.getInput}
                  title="Add ToDo"
                  disabled={this.props == ''}
                />
              </View>
              <View style={{ marginBottom: 10 }}>
                <Button
                  style={{ flex: 1, height: 0 }}
                  onPress={this.onPressRemoveButton}
                  title="Remove"
                  disabled={this.getCompletedToDoCount() == 0}
                />
              </View>

              <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                  {this.props.todos.list.map((item, index) => {
                    return <CheckBox
                      key={item.id}
                      style={{ flex: 1, padding: 5 }}
                      onClick={() => this.toggleTodo(item)}
                      isChecked={item.complete}
                      rightText={item.text}
                    />
                  })}
                </View>
              </ScrollView>
            </View >
          ) : (
              <Text style={{ fontSize: 20 }}>Cannot connect to server.</Text>
            )
        )}
      </ConnectivityRenderer>
    );
  }
}

// map app state to the class props
export default connect(({ todos }) => ({ todos }), { createTodo, updateTodo, deleteTodo, getTodos })(ToDoList);