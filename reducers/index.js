import todos from '../todos/reducer';
import { reducer as network } from 'react-native-offline';
import {
  combineReducers
} from 'redux';

export default combineReducers({ todos, network });